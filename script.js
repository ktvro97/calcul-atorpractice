//set all variables
//dont need more than 2 numbers because at end it resets one so we arent stuck with a million numbers
var num1= 0 ;
var num2 = 0 ;
var operator= 0 ;
var total = 0;

//brings in jquery document and its functions
//once the click loads, so do its funtions
// button reads the html
// if you click any number 0-9 brings you to number function
//if you click anything else it brings you to operator function
$(document).ready(function() {
    $('button').on('click', function(a) {
        var btn = a.target.innerHTML;
        if (btn >= '0' && btn <= '9') {
            numberPressed(btn);
        } 
        else {
            operaterButtonPressed(btn);
        }
    });
});

//if the first number pressed is 0 set number 1 to 0
//if not set 2 to 0
//display the number that was pressed
function numberPressed(num) {
    if (num1 === 0 ) {
        num1 = num;
    } 
    else {
        num2 = num;
    }
    displayButton(num);
}

//if operator button thats pressed equals 0 then set the operator to that
//if not the go to the alltogether function---should go to this
function operaterButtonPressed(operation) {
    if (operator === 0) {
        operator = operation;
    } 
    else {
        allTogether();
        operator = operation;
    }
}

//if + is pressed add the numbers
//if - is pressed subtract the numbers
// if / is pressed divide the numbers
// if x is pressed * the  numbers
//if . is pressed concatenate the numbers with a . in the middle
// display after each function
//when AC is pressed set everything to 0 and start over---- doesnt work
function allTogether() {
    switch (operator) {
        case '+':
            total = +num1 + +num2;
            displayButton(total);
            break;
        case '-':
            total = +num1 - +num2;
            displayButton(total);
            break;
        case '/':
            total = +num1 / +num2;
            displayButton(total);
            break;
        case 'X':
            total = +num1 * +num2;
            displayButton(total);
            break;
        case '.':
            total= num1 + "."+ num2;
            displayButton(total);
            break;
    }
    updateVariables();
}


//displays the result when buttons are pressed
function displayButton(btn) {
    $(".calc-result-input").text(btn);
}

//updates at the end of each round of switch cases
function updateVariables() {
    num1 = total;
    num2 = 0;
}


function myReset() {
    $("#my-calculator").reset();
}

